#!/bin/bash

# Install & configure Apache

yum install -y httpd http-devel httpd-tools
chkconfig --add httpd
chkconfig httpd on
service httpd stop

rm -rf /var/www/html

ln -s /vagrant /var/www/html

service httpd start

# Install PHP

yum install -y php php-cli php-common php-devel php-mysql

# Download Starter content

cd /vagrant
sudo -u vagrant wget -q https://bitbucket.org/yogesh29aus/vagrant/raw/84d14072347ac9c24137465bfd10b0359b779efc/files/index.html
sudo -u vagrant wget -q https://bitbucket.org/yogesh29aus/vagrant/raw/84d14072347ac9c24137465bfd10b0359b779efc/files/info.php

service httpd restart
